package com.company;

interface UpdateProductDao {

    boolean all(Product product);

    boolean name(Product product);

    boolean unitPrice(Product product);

    boolean quantity(Product product);

    boolean importDate(Product product);
}
